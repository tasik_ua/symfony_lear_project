<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Genus;
use Doctrine\ORM\EntityRepository;

class FunFamilyRepository extends EntityRepository
{
    public function createQueryBuilderAlphabetical()
    {
        return $this->createQueryBuilder('fun_family')
            ->orderBy('fun_family.name', 'ASC');
    }
}

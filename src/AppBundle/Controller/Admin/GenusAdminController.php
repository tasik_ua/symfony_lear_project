<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Genus;

/**
 * @Security("is_granted('ROLE_MANAGE_GENUS')")
 * @Route("/admin")
 */
class GenusAdminController extends Controller
{
    /**
     *
     * @Route("/genus", name="admin_genus_list")
     */
    public function indexAction()
    {
        $genuses = $this->getDoctrine()
            ->getRepository('AppBundle:Genus')
            ->findAll();

        return $this->render('admin/genus/list.html.twig', array(
            'genuses' => $genuses
        ));
    }

    /**
     * @Route("/genus/new", name="admin_genus_new")
     */
    public function newAction(Request $request)
    {
        // let's go to work!
        $form = $this->createForm('AppBundle\Form\GenusFormType');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $genus = $form->getData();

            $em = $this->getDoctrine()->getManager();

            $em->persist($genus);

            $em->flush();

            $this->addFlash('success', sprintf('Yahoo, I added new genus, %s', $this->getUser()));

            return $this->redirectToRoute('admin_genus_list');
        }

        return $this->render('admin/genus/new.html.twig', [
            'formGenus' => $form->createView(),
        ]);
    }

    /**
     * @Route("/genus/{id}/edit", name="admin_genus_edit")
     */
    public function editAction(Request $request, Genus $genus)
    {
        // let's go to work!
        $form = $this->createForm('AppBundle\Form\GenusFormType', $genus);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $genus = $form->getData();

            $em = $this->getDoctrine()->getManager();

            $em->persist($genus);

            $em->flush();

            $this->addFlash('success', 'Yahoo, I updated genus!');

            return $this->redirectToRoute('admin_genus_list');
        }

        return $this->render('admin/genus/edit.html.twig', [
            'formGenus' => $form->createView(),
            'genus' => $genus
        ]);
    }
}
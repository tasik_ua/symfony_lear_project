<?php
/**
 * Created by PhpStorm.
 * User: tasik
 * Date: 13.07.17
 * Time: 22:37
 */

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TestController extends Controller
{
    /**
     * @Route("/test")
     */
    public function indexAction()
    {
        die('test');
    }
}